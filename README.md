
# Serverteil der 'xMZ-Plattform'

|||
|:---|:------|
|**master:**|[![Build Status](https://gitlab.com/RA-GAS-GmbH/xmz-server/badges/master/build.svg)](https://gitlab.com/RA-GAS-GmbH/xmz-server/pipelines)&nbsp;[![Code Coverage](https://codecov.io/gh/Kliemann-Service-GmbH/xmz-server/branch/master/graph/badge.svg)](https://codecov.io/gh/Kliemann-Service-GmbH/xmz-server)|
|**development:**|[![Build Status](https://gitlab.com/RA-GAS-GmbH/xmz-server/badges/development/build.svg)](https://gitlab.com/RA-GAS-GmbH/xmz-server/pipelines)&nbsp;[![Code Coverage](https://codecov.io/gh/Kliemann-Service-GmbH/xmz-server/branch/development/graph/badge.svg)](https://codecov.io/gh/Kliemann-Service-GmbH/xmz-server)|


## Dokumentation

-   [API Dokumentation master][doc-api-master]
-   [Repository][repo-xmz-server]
-   [Homepage][homepage]

## Quellcode auschecken

Dieses Repository ist Teil der [xMZ-Plattform][repo-xmz-plattform]. Es wird empfohlen alle Komponenten
auszuchecken.

Der Quellcode kann mit folgendem Git Befehl herunter geladen werden.
Der Parameter `--recursive` sorgt dafür das alle Komponenten (git submodule)
ebenfalls herunter geladen werden.

```bash
git clone --recursive https://gitlab.com/RA-GAS-GmbH/xMZ-Plattform.git
```

Alternative kann dieser Teil auch einzeln heruntergeladen werden.

```bash
git clone https://gitlab.com/RA-GAS-GmbH/xmz-server.git
```



# Links

-   <https://gitlab.com/RA-GAS-GmbH/xMZ-Plattform>
-   <https://ra-gas-gmbh.gitlab.io/xmz-server/xmz_server>


[homepage]: https://ra-gas-gmbh.gitlab.io/xmz-server/xmz_server/
[doc-api-master]: https://ra-gas-gmbh.gitlab.io/xmz-server/xmz_server/
[repo-xmz-plattform]: https://gitlab.com/RA-GAS-GmbH/xMZ-Plattform
[repo-xmz-server]: https://gitlab.com/RA-GAS-GmbH/xmz-server
